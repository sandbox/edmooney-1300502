<?php

/**
 * @file
 * untappd api functions.
 *
 */
define("UNTAPPD_API_ENDPOINT","http://api.untappd.com/v3/");


/**
 * Future use: Title callback.
 */
function untappd_titles($action){
  switch ($action){
    case 'thepub':
      return t('Public Feed');
    case 'user_feed':
      return t('User Feed');
    case 'trending':
      return t('Trending');
  }
}

/**
 * Abstraction function for calling private function.
 */
function untappd_actions($action, $vars = ''){  
   $action = strtolower(trim($action));
   $res = _untappd_request($action, $vars);
   return $res;
}

/**
 * Future: Helper function for URL params
 */
function _makeparams($key,$value){
    return urlencode($key).'='.urlencode($value);
}


/**
 * Worker function.
 *
 */
function _untappd_request($action, $params){
  //$pass = md5();
  $key = variable_get('untappd_api_key');
  if (!$key) {
    //TODO
  }
  if (empty($params)){
  $req_url = UNTAPPD_API_ENDPOINT.$action.'?key='.$key;
  //TODO: auth: http://username:password@api.untappd.com/
  }
  else{
    $req_url = UNTAPPD_API_ENDPOINT.$action.'?key='.$key.'&'.$params;
  }

  $res = drupal_http_request($req_url);
  if ($res->code == 200) {
    return json_decode($res->data, true);
  }

  return FALSE;
}
