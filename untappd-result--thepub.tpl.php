<div class="untappd-thepub">
<?php
/**
 * $ut represents the results set you can then use this result set to format data to your needs
 * see http://untappd.com/api/ for more infomation on the data structure.
 * use print_r($ut) for more details of output
 * 
 * example: $ut['results']
 */
?>
<ul>
<?php
foreach ($ut['results'] as $r){
	$avatarimg = $r['user']['user_avatar'];
	$username = $r['user']['user_name'];
	$bio = $r['user']['bio'];
	$beer = $r['beer_name'];
	?>
		<li>
			<img class="thepub-avatar" src="<?php print $avatarimg; ?>"/>
			<p class="thepub-user"><a href="http://untappd.com/user/<?php print $username;?>"><?php print $username;?></a></p>
			<? if (!empty($bio)){ ?>
			<p>Bio: <?php print $bio;?></p>
			<? } ?>
			<p>Beer: <?php print $beer;?></p>
		</li>
	<?
}
?>
</ul>
</div>
