<?php

/**
 * hook_form() - admin setting form
 */
function untappd_config_settings($form, &$form_state){
  $form['untappd_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#required' => TRUE,
    '#default_value' => variable_get('untappd_api_key',''),
    '#description' => t('Untappd API key value'),
  );
  $form['untappd_user_profile'] = array(
    '#title'=>'Untappd User Profile',
    '#type'=>'select',
    '#default_value' => variable_get('untappd_user_profile','1'),
    '#options' => array(
      '1'=>t('Yes'),
      '0'=>t('No')
     ), 
    '#description'=>t('Enable Untappd Username setting for User Profiles?'),
  );
    
 return system_settings_form($form);
}
